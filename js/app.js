function cargarDatos(){
    const url = '/html/alumnos.json';
    axios
    .get(url)
    .then((res)=>{
        mostrar(res.data)
    }).catch((err)=>{
        console.log("Acaba de surgir un error");
    })
    function mostrar(data){
        const res = document.getElementById('Respuesta');
        res.innerHTML = "";
        const ProGe = document.getElementById('Respuesta2');
        ProGe.innerHTML = "";
        let i = 0;
        var ProTotal = 0;
        
        for(item of data){
            const Pro = (item.matematicas + item.quimica + item.fisica + item.geografia) / 4; 
            i++;
            ProTotal = ProTotal + Pro;
            res.innerHTML += `
            <tr>
              <td>${item.id}</td>
              <td>${item.matricula}</td>
              <td>${item.nombre}</td>
              <td>${item.matematicas}</td>
              <td>${item.quimica}</td>
              <td>${item.fisica}</td>
              <td>${item.geografia}</td>
              <td>${Pro.toFixed(2)}</td>
            </tr>
          `;
        }
        ProGe.innerHTML += "<div id=tabla2>" +  "Promedio General: " + (ProTotal/i).toFixed(2) + "</div><br>";
    }
}

const res = document.getElementById('btnMostrar');
const lim = document.getElementById('btnLimpiar');

res.addEventListener('click', function(){
    cargarDatos();
})

lim.addEventListener('click', function(){
    const res = document.getElementById('Respuesta');
    res.innerHTML = "";
    const ProGe = document.getElementById('Respuesta2');
    ProGe.innerHTML = "";
})